//
// Created by mmaximo on 25/04/16.
//

#ifndef CT204_LOCALIZATION_MATHUTILS_H
#define CT204_LOCALIZATION_MATHUTILS_H

#include <random>

/**
 * Classe com funcoes matematicas auxiliares.
 */
class MathUtils {
private:
    static std::default_random_engine generator;

public:
    /**
     * Normaliza um valor de angulo para que fique no intervalo [-pi,pi).
     *
     * @param randians angulo em radianos.
     */
    static double normalizeAngle(double radians);
    /**
     * Gera um numero aleatorio usando distribuicao gaussiana.
     *
     * @param mean media da distribuicao.
     * @param sigma desvio padrao da distribuicao.
     * @return numero aleatorio distribuido conforme N(mean, sigma^2).
     */
    static double generateGaussianRandomNumber(double mean, double sigma);
    /*
     * Gera um numero aleatorio usando distribuicao uniforme.
     *
     * @param min limite inferior do intervalo da distribuicao.
     * @param max limite superior do intervalo da distribuicao.
     * @return numero aleatorio entre min e max com distribuicao uniforme.
     */
    static double generateUniformRandomNumber(double min, double max);
};

#endif //CT204_LOCALIZATION_MATHUTILS_H
