//
// Created by mmaximo on 25/04/16.
//

#ifndef CT204_LOCALIZATION_POSE2D_H
#define CT204_LOCALIZATION_POSE2D_H

/**
 * Classe que representa uma postura (posicao e orientacao) no plano horizontal.
 */
struct Pose2D {
    double x;
    double y;
    double psi;

    /**
     * Construtor de Pose2D.
     *
     * @param x coordenada x.
     * @param y coordenada y.
     * @param psi angulo de guinada (yaw).
     */
    Pose2D(double x, double y, double psi);

    /**
     * Construtor padrao de Pose2D.
     * Inicializa x, y e psi como zero.
     */
    Pose2D();
};

#endif //CT204_LOCALIZATION_POSE2D_H