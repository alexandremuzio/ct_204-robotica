//
// Created by mmaximo on 25/04/16.
//

#ifndef CT204_LOCALIZATION_MONTECARLOLOCALIZATION_H
#define CT204_LOCALIZATION_MONTECARLOLOCALIZATION_H

#include "ct204_localization/Pose2D.h"
#include "ct204_localization/Particle.h"

/**
 * Classe para armazenar parametros para configuracao da Localizacao de Monte Carlo.
 */
struct MonteCarloLocalizationParams {
    int numParticles;
    double initialXCovariance;
    double initialYCovariance;
    double initialPsiCovariance;
    double alpha1;
    double alpha2;
    double alpha3;
    double alpha4;

    /**
     * Cria um objeto com parametros padrao para a Localizacao de Monte Carlo.
     */
    static MonteCarloLocalizationParams getDefaultParams();
};

/**
 * Classe que representa a Localizacao de Monte Carlo.
 */
class MonteCarloLocalization {
private:
    Particle* particles;
    Pose2D estimate;
    MonteCarloLocalizationParams params;

public:
    /**
     * Construtor de MonteCarloLocalization.
     *
     * @param params parametros para configuracao de MonteCarloLocalization.
     */
    MonteCarloLocalization(MonteCarloLocalizationParams params);
    /**
     * Destrutor.
     */
    ~MonteCarloLocalization();
    /**
     * Inicializa a Localizacao de Monte Carlo a partir de uma estimativa inicial da postura do robo.
     *
     * @param initialPose estimativa inicial da postura do robo.
     */
    void initialize(Pose2D initialPose);
    /**
     * Realiza passo de predicao.
     *
     * @param linearSpeed velocidade linear do robo.
     * @param angularSpeed velocidade angular do robo.
     * @param elapsedTime tempo decorrido desde a ultima predicao.
     */
    void predict(double linearSpeed, double angularSpeed, double elapsedTime);
    /**
     * Calcula a estimativa a partir das particulas.
     */
    void computeEstimate();
    /**
     *
     */
    Particle* getParticles();
    const Pose2D& getEstimate();
    int getNumParticles();
};

#endif //CT204_LOCALIZATION_MONTECARLOLOCALIZATION_H
