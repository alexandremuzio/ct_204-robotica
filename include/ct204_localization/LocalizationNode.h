//
// Created by mmaximo on 25/04/16.
//

#ifndef CT204_LOCALIZATION_LOCALIZATIONNODE_H
#define CT204_LOCALIZATION_LOCALIZATIONNODE_H

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/LaserScan.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "ct204_localization/MonteCarloLocalization.h"
#include "geometry_msgs/PoseArray.h"

class LocalizationNode {
private:
    MonteCarloLocalization *localization;
    double timeLastPredict;
    geometry_msgs::PoseArray particlesPoseArray;

    ros::Subscriber odometrySubscriber;
    ros::Subscriber scanSubscriber;
    ros::Subscriber initialPoseSubscriber;
    ros::Publisher particlesPublisher;
    ros::Publisher estimatePublisher;

    void debugOdometryCallback(const nav_msgs::Odometry::ConstPtr &odometryMsg);
    void debugScanCallback(const sensor_msgs::LaserScan::ConstPtr &scanMsg);
    void debugInitialPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &initialPoseMsg);
    void publishData();

public:
    LocalizationNode();
    ~LocalizationNode();

    /**
     * Callback para tratar mensagens do topico /initialpose.
     *
     * @param initialPoseMsg mensagem do topico /initialpose.
     */
    void initialPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &initialPoseMsg);
    /**
     * Callback para tratar mensagens de odometria.
     *
     * @param odometryMsg mensagem de odometria.
     */
    void odometryCallback(const nav_msgs::Odometry::ConstPtr &odometryMsg);
    /**
     * Callback para tratar mensagens de scan (Kinect).
     *
     * @param scanMsg mensagem de scan.
     */
    void scanCallback(const sensor_msgs::LaserScan::ConstPtr &scanMsg);
};

#endif //CT204_LOCALIZATION_LOCALIZATIONNODE_H
