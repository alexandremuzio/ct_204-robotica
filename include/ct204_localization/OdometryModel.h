//
// Created by mmaximo on 25/04/16.
//

#ifndef CT204_LOCALIZATION_ODOMETRYMODEL_H
#define CT204_LOCALIZATION_ODOMETRYMODEL_H

#include "ct204_localization/Pose2D.h"

/**
 * Classe que representa o modelo de odometria de um robo diferencial.
 */
class OdometryModel {
public:
    /**
     * Funcao que propaga a postura do robo com base nas velocidades linear e angular.
     */
    static Pose2D propagate(Pose2D currentPose, double linearSpeed, double angularSpeed, double elapsedTime);
private:
    static double xSigma;
    static double ySigma;
    static double psiSigma;
};

#endif //CT204_LOCALIZATION_ODOMETRYMODEL_H
