//
// Created by mmaximo on 25/04/16.
//

#ifndef CT204_LOCALIZATION_CLOCK_H
#define CT204_LOCALIZATION_CLOCK_H

/**
 * Classe para medicao de tempo de execucao.
 */
class Clock {
private:
    double timeLastTic;

public:
    /**
     * Construtor padrao.
     */
    Clock();
    /**
     * Retorna o tempo atual em segundos.
     *
     * @return tempo atual em segundos.
     */
    static double getCurrentTime();
    /**
     * Inicia um cronometro. Funcionamento semelhante a funcao tic() do MATLAB.
     * Para medir tempo de uma certa porcao de codigo, faca o seguinte:
     *
     * Clock c;
     * c.tic();
     * // Porcao de codigo cujo tempo deseja-se medir.
     * double t = c.toc();
     */
    void tic();
    /**
     * Retorna o tempo decorrido desde o ultimo tic().
     *
     * @return tempo decorrido desde o ultimo tic().
     */
    double toc();
};

#endif //CT204_LOCALIZATION_CLOCK_H
