//
// Created by mmaximo on 25/04/16.
//

#ifndef CT204_LOCALIZATION_PARTICLE_H
#define CT204_LOCALIZATION_PARTICLE_H

#include "ct204_localization/Pose2D.h"

/**
 * Classe que representa uma particula de um filtro de particulas.
 */
struct Particle {
    Pose2D pose;
    double weight;

    /**
     * Construtor padrao de Particle.
     */
    Particle();
};

#endif //CT204_LOCALIZATION_PARTICLE_H
