//
// Created by mmaximo on 25/04/16.
//

#include "ros/ros.h"
#include "tf/tf.h"
#include "ct204_localization/LocalizationNode.h"

LocalizationNode::LocalizationNode() : timeLastPredict(-1.0) {
    ros::NodeHandle nodeHandle;

    MonteCarloLocalizationParams params = MonteCarloLocalizationParams::getDefaultParams();
    localization = new MonteCarloLocalization(params);

    // Inicializacao de variavel
    for (int i = 0; i < params.numParticles; ++i)
        particlesPoseArray.poses.push_back(geometry_msgs::Pose());

    // Criacao dos subscribers
    odometrySubscriber = nodeHandle.subscribe("/odom", 1000, &LocalizationNode::odometryCallback, this);
    scanSubscriber = nodeHandle.subscribe("/scan", 1000, &LocalizationNode::scanCallback, this);
    initialPoseSubscriber = nodeHandle.subscribe("/initialpose", 1000, &LocalizationNode::initialPoseCallback, this);

    // Criacao dos publishers
    particlesPublisher = nodeHandle.advertise<geometry_msgs::PoseArray>("/localization/particles", 1);
    estimatePublisher = nodeHandle.advertise<geometry_msgs::PoseStamped>("/localization/estimate", 1);

    ros::spin();
}

LocalizationNode::~LocalizationNode() {
    delete localization;
}

void LocalizationNode::initialPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &initialPoseMsg) {
    debugInitialPoseCallback(initialPoseMsg);

    Pose2D initialPose;
    initialPose.x = initialPoseMsg->pose.pose.position.x;
    initialPose.y = initialPoseMsg->pose.pose.position.y;
    initialPose.psi = tf::getYaw(initialPoseMsg->pose.pose.orientation);
    localization->initialize(initialPose);

    publishData();
}

void LocalizationNode::odometryCallback(const nav_msgs::Odometry::ConstPtr &odometryMsg) {
    debugOdometryCallback(odometryMsg);

    // Calcula tempo decorrido desde a ultima mensagem de odometria
    double elapsedTime;
    double time = odometryMsg->header.stamp.toSec();
    if (timeLastPredict < 0.0)
        elapsedTime = 0.0;
    else
        elapsedTime = time - timeLastPredict;

    double v = odometryMsg->twist.twist.linear.x;
    double w = odometryMsg->twist.twist.angular.z;
    localization->predict(v, w, elapsedTime);
    localization->computeEstimate();

    ROS_INFO("elapsedTime = %f\n", elapsedTime);
    // Publica posturas das particulas e da estimativa
    timeLastPredict = time;
    publishData();
}

void LocalizationNode::scanCallback(const sensor_msgs::LaserScan::ConstPtr &scanMsg) {
    debugScanCallback(scanMsg);

    // Implementar passo de filtragem.

    publishData();
}

void LocalizationNode::debugInitialPoseCallback(
        const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &initialPoseMsg) {

    double initialX = initialPoseMsg->pose.pose.position.x;
    double initialY = initialPoseMsg->pose.pose.position.y;
    double initialPsi = tf::getYaw(initialPoseMsg->pose.pose.orientation);

    ROS_INFO( "Initial Pose: [%f %f %f]\n", initialX, initialY , initialPsi);
}

void LocalizationNode::debugOdometryCallback(const nav_msgs::Odometry::ConstPtr &odometryMsg) {
    double linearX = odometryMsg->twist.twist.linear.x;
    double linearY = odometryMsg->twist.twist.linear.y;
    double linearZ = odometryMsg->twist.twist.linear.z;
    double v = sqrt(linearX * linearX + linearY * linearY + linearZ * linearZ);

    double w = odometryMsg->twist.twist.angular.z;

    ROS_INFO("Odometry information: [%f %f]\n", v, w);
}

void LocalizationNode::debugScanCallback(const sensor_msgs::LaserScan::ConstPtr &scanMsg) {
    double scanSum = 0.0;
    double scanNum = scanMsg->ranges.size();

    if (scanNum == 0) return;

    for (auto& r : scanMsg->ranges) {
        if (std::isnan(r)) continue;
        scanSum += r;
    }
    double mean = scanSum / scanNum;
    ROS_INFO("Scan Mean Range: %f\n", mean);
}

void LocalizationNode::publishData() {
    // Criacao de mensagem com posturas das particulas
    int numParticles = localization->getNumParticles();
    Particle* particles = localization->getParticles();
    for (int i = 0; i < numParticles; ++i) {
        particlesPoseArray.poses[i].position.x = particles[i].pose.x;
        particlesPoseArray.poses[i].position.y = particles[i].pose.y;
        particlesPoseArray.poses[i].position.z = 0.0;
        particlesPoseArray.poses[i].orientation = tf::createQuaternionMsgFromYaw(particles[i].pose.psi);
    }

    // Criacao de mensagem com postura da estimativa
    Pose2D estimate = localization->getEstimate();
    geometry_msgs::PoseStamped estimateMsg;
    estimateMsg.pose.position.x = estimate.x;
    estimateMsg.pose.position.y = estimate.y;
    estimateMsg.pose.position.z = 0.0;
    estimateMsg.pose.orientation = tf::createQuaternionMsgFromYaw(estimate.psi);

    // Configuracao dos headers das mensagens
    particlesPoseArray.header.stamp = ros::Time::now();
    particlesPoseArray.header.frame_id = "/map";
    estimateMsg.header.stamp = ros::Time::now();
    estimateMsg.header.frame_id = "/map";

    // Publicacao das mensagens
    particlesPublisher.publish(particlesPoseArray);
    estimatePublisher.publish(estimateMsg);
}