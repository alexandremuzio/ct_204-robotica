//
// Created by mmaximo on 25/04/16.
//

#include "ct204_localization/Pose2D.h"

Pose2D::Pose2D(double x, double y, double psi) : x(x), y(y), psi(psi) {

}

Pose2D::Pose2D() : Pose2D(0.0, 0.0, 0.0) {

}