//
// Created by mmaximo on 25/04/16.
//

#include <math.h>
#include "ct204_localization/MathUtils.h"

std::default_random_engine MathUtils::generator;

double MathUtils::normalizeAngle(double radians) {
    while (radians < -M_PI)
        radians += 2.0 * M_PI;
    while (radians >= M_PI)
        radians -= 2.0 * M_PI;
    return radians;
}

double MathUtils::generateGaussianRandomNumber(double mean, double sigma) {
    std::normal_distribution<double> normal(mean, sigma);
    return normal(generator);
}

double MathUtils::generateUniformRandomNumber(double min, double max) {
    std::uniform_real_distribution<double> uniform(min, max);
    return uniform(generator);
}
