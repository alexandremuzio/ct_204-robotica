//
// Created by mmaximo on 25/04/16.
//

#include <math.h>
#include "ct204_localization/OdometryModel.h"
#include "ct204_localization/MathUtils.h"

double OdometryModel::xSigma = 0.001;
double OdometryModel::ySigma = 0.001;
double OdometryModel::psiSigma = 0.005;

Pose2D OdometryModel::propagate(Pose2D currentPose, double linearSpeed, double angularSpeed, double elapsedTime) {
    Pose2D newPose;

    newPose.x = currentPose.x + 2 * linearSpeed / angularSpeed * sin(angularSpeed * elapsedTime / 2)
                                * cos(currentPose.psi + angularSpeed * elapsedTime / 2)
                + MathUtils::generateGaussianRandomNumber(0, xSigma);
    newPose.y = currentPose.y + 2 * linearSpeed / angularSpeed * sin(angularSpeed * elapsedTime / 2)
                                * sin(currentPose.psi + angularSpeed * elapsedTime / 2)
                + MathUtils::generateGaussianRandomNumber(0, ySigma);
    newPose.psi = currentPose.psi + angularSpeed * elapsedTime
                    + MathUtils::generateGaussianRandomNumber(0, psiSigma);

    return newPose;
}