//
// Created by mmaximo on 25/04/16.
//

#include "ct204_localization/MonteCarloLocalization.h"
#include "ct204_localization/MathUtils.h"
#include "ct204_localization/OdometryModel.h"

MonteCarloLocalizationParams MonteCarloLocalizationParams::getDefaultParams() {
    MonteCarloLocalizationParams params;
    params.numParticles = 200;
    params.initialXCovariance = 1.0;
    params.initialYCovariance = 1.0;
    params.initialPsiCovariance = (M_PI / 12.0) * (M_PI / 12.0);
    params.alpha1 = 0.2; // relativo ao modelo da variancia da velocidade linear
    params.alpha2 = 0.2; // relativo ao modelo da variancia da velocidade linear
    params.alpha3 = 0.2; // relativo ao modelo da variancia da velocidade angular
    params.alpha4 = 0.2; // relativo ao modelo da variancia da velocidade angular
    return params;
}

MonteCarloLocalization::MonteCarloLocalization(MonteCarloLocalizationParams params) : params(params) {
    particles = new Particle[params.numParticles];
    initialize(Pose2D());
}

MonteCarloLocalization::~MonteCarloLocalization() {
    delete[] particles;
}

void MonteCarloLocalization::initialize(Pose2D initialPose) {
    for (int i = 0; i < params.numParticles; i++) {
        particles[i].pose.x = MathUtils::generateGaussianRandomNumber(initialPose.x, params.initialXCovariance);
        particles[i].pose.y = MathUtils::generateGaussianRandomNumber(initialPose.y, params.initialYCovariance);
        particles[i].pose.psi = MathUtils::generateGaussianRandomNumber(initialPose.psi, params.initialPsiCovariance);
    }
}

void MonteCarloLocalization::predict(double linearSpeed, double angularSpeed, double elapsedTime) {
    for(int i = 0; i < params.numParticles; i++) {
        particles[i].pose = OdometryModel::propagate(particles[i].pose, linearSpeed, angularSpeed, elapsedTime);
    }
}

void MonteCarloLocalization::computeEstimate() {
    double xSum = 0.0;
    double ySum = 0.0;
    double sumSine = 0.0;
    double sumCossine = 0.0;
    for (int i = 0; i < params.numParticles; i++) {
        xSum += particles[i].pose.x;
        ySum += particles[i].pose.y;
        sumSine += sin(particles[i].pose.psi);
        sumCossine += cos(particles[i].pose.psi);
    }

    estimate.x = xSum / params.numParticles;
    estimate.y = ySum / params.numParticles;
    estimate.psi = atan2(sumSine, sumCossine);
}

Particle* MonteCarloLocalization::getParticles() {
    return particles;
}

const Pose2D& MonteCarloLocalization::getEstimate() {
    return estimate;
}

int MonteCarloLocalization::getNumParticles() {
    return params.numParticles;
}