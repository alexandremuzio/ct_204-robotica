//
// Created by mmaximo on 27/04/16.
//

#include <iostream>
#include <fstream>
#include <math.h>
#include "ct204_localization/OdometryModel.h"

int main() {
    std::ofstream file("trajectory.txt");

    double linearSpeed = 0.2;
    double angularSpeed = (M_PI / 2.0);

    Pose2D currentPose = Pose2D(0.0, 0.0, 0.0);

    double dt = 0.01;
    double time = 0.0;
    double maxTime =4.0;
    for (; time < maxTime; time += dt) {
        currentPose = OdometryModel::propagate(currentPose, linearSpeed, angularSpeed, dt);
        file << currentPose.x << " " << currentPose.y << " " << currentPose.psi << std::endl;
    }

    file.flush();
    file.close();

    return 0;
}