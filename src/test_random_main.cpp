//
// Created by mmaximo on 27/04/16.
//

#include <fstream>
#include "ct204_localization/MathUtils.h"

int main() {
    std::ofstream file("random.txt");

    double mean = 1;
    double sigma = 2;
    double randomNumber = 0.0;
    for (int i = 0; i < 1000000; ++i) {
        randomNumber = MathUtils::generateGaussianRandomNumber(mean, sigma);
        file << randomNumber << std::endl;
    }

    file.flush();
    file.close();

    return 0;
}